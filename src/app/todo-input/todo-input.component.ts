import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.css']
})
export class TodoInputComponent implements OnInit {
  newTodoItem: string;
  // @Output() exportNewTodoItem = new EventEmitter<string>();

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
  }

  addNewTodoItem() {
    // console.log('เพิ่มรายการที่ต้องทำใหม่');
    // this.exportNewTodoItem.emit(this.newTodoItem);
    this.todoService.addNew(this.newTodoItem);
    this.newTodoItem = '';
  }

}
