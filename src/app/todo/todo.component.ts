import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TodoService } from '../todo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit , OnDestroy {
  todoList:string[];
  todoSubject:Subscription;

  constructor(private todoService: TodoService) {

  }

  ngOnInit(): void {
    this.todoList = this.todoService.todoItem;
    this.todoSubject = this.todoService.todoItemChange.subscribe(todoItem =>{
      this.todoList = todoItem;
    });
  }

  onRemoveTodoFromList(todo:string) {
    this.todoService.removeTodoFromList(todo);
  }

  ngOnDestroy() {
    this.todoSubject.unsubscribe();
  }
}
