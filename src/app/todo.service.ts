import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoItemChange = new Subject<string[]>();
  todoItem:string[] = ['รายการที่ต้องทำ1','รายการที่ต้องทำ2'];

  constructor() { }

  addNew(newTodoItem:string) {
    this.todoItem.push(newTodoItem);
    console.log(this.todoItem);
    this.todoItemChange.next(this.todoItem);

  }

  removeTodoFromList(willbeRemove:string) {
    this.todoItem = this.todoItem.filter( eachItem => {
      return willbeRemove !== eachItem;
    });
    this.todoItemChange.next(this.todoItem);
  }
}
